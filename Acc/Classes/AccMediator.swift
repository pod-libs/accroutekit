//
//  SwiftyMediator.swift
//  Acc
//
//  Created by zhouen on 2021/1/8.
//  Copyright (c) 2019 zhouen. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//


#if os(iOS) || os(tvOS)
import UIKit
#endif

public let Acc = AccMediator.shared


open class AccMediator {
    static let shared = AccMediator()
    private init() {}
    
    private var routeTargets: [RoutableTargetType.Type] = []
    private var replacePatterns: [String: URLConvertible] = [:]
    private var moduleSevices: [String: AccServiceProtocol.Type] = [:]
    
    
    public func register(_ targetType: RoutableTargetType.Type) {
        self.routeTargets.append(targetType)
    }
    
    public func replace(url: URLConvertible, with replacer: URLConvertible) {
        self.replacePatterns[url.pattern] = replacer
    }
    
    public func registerSevices<T: AccServiceProtocol, M>(_ moduleType: T.Type, moduleSevice: M.Type) {
        let key = String(describing: moduleSevice)
        self.moduleSevices[key] = moduleType
    }
    
    public func getSevice<M>(_ moduleSevice: M.Type) -> M?{
        let key = String(describing: moduleSevice)
        if let moduleType = moduleSevices[key] {
            return moduleType.service() as? M
        }
        return nil
    }
}


extension AccMediator {
    
    public func viewController(of url: URLConvertible) -> UIViewController? {
        let url = self.replacePatterns[url.pattern] ?? url
        guard let target = self.targetType(of: url) else { return nil }
        return self.viewController(of: target)
    }
    
    @discardableResult
    public func push(_ url: URLConvertible, from: UINavigationController? = nil, animated: Bool = true) -> UIViewController? {
        guard let target = self.targetType(of: url) else { return nil }
        return self.push(target, from: from, animated: animated)
    }
    
    @discardableResult
    public func present(_ url: URLConvertible, from: UIViewController? = nil, wrap: UINavigationController.Type? = nil, animated: Bool = true, completion: (() -> Void)? = nil) -> UIViewController? {
        guard let target = self.targetType(of: url) else { return nil }
        return self.present(target, from: from, wrap: wrap, animated: animated, completion: completion)
    }
    
    
    @discardableResult
    public func targetType(of url: URLConvertible) -> MediatorTargetType? {
        let url = self.replacePatterns[url.pattern] ?? url
        guard let routable = routeTargets.compactMap({ $0.init(url: url) }).first else { return nil  }
        return routable
    }
    
}


extension AccMediator {

    @discardableResult
    public func push(_ target: MediatorTargetType, from: UINavigationController? = nil, animated: Bool = true) -> UIViewController? {
        guard let viewController = self.viewController(of: target) else { return nil }
        guard let navigationController = from ?? ls_getTopVC()?.navigationController else { return nil }
        navigationController.pushViewController(viewController, animated: animated)
        return viewController
    }
    
    @discardableResult
    public func present(_ target: MediatorTargetType, from: UIViewController? = nil, wrap: UINavigationController.Type? = nil, animated: Bool = true, completion: (() -> Void)? = nil) -> UIViewController? {
        guard let viewController = self.viewController(of: target) else { return nil }
        guard let fromViewController = from ?? ls_getTopVC() else { return nil }
        
        let viewControllerToPresent: UIViewController
        if let navigationControllerClass = wrap, (viewController is UINavigationController) == false {
            viewControllerToPresent = navigationControllerClass.init(rootViewController: viewController)
        } else {
            viewControllerToPresent = viewController
        }
        
        fromViewController.present(viewControllerToPresent, animated: animated, completion: completion)
        return viewController
    }
    
    public func viewController(of target: MediatorTargetType) -> UIViewController? {
        guard let t = target as? RoutableTargetType else {
            print("MEDIATOR WARNINIG: \(target) does not conform to MediatorSourceType")
            return nil
        }
        guard let viewController = t.viewController else { return nil }
        return viewController
    }
    

}
