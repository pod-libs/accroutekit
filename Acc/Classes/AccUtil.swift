//
//  UIViewController+TopMost.swift
//  SwiftyMediator
//
//  Created by zhouen on 2021/1/8.
//  Copyright (c) 2019 zhouen. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#if os(iOS) || os(tvOS)
import UIKit
#endif


// MARK: - 查找顶层控制器、
// 获取顶层控制器 根据window
func ls_getTopVC() -> UIViewController? {
    var window = UIApplication.shared.keyWindow
    //是否为当前显示的window
    if window?.windowLevel != UIWindow.Level.normal{
        let windows = UIApplication.shared.windows
        for  windowTemp in windows{
            if windowTemp.windowLevel == UIWindow.Level.normal{
                window = windowTemp
                break
            }
        }
    }
    let vc = window?.rootViewController
    return ls_getTopVC(withCurrentVC: vc)
}
///根据控制器获取 顶层控制器
func ls_getTopVC(withCurrentVC vc :UIViewController?) -> UIViewController? {
    if vc == nil {
        print("🌶： 找不到顶层控制器")
        return nil
    }
    if let presentVC = vc?.presentedViewController {
        //modal出来的 控制器
        return ls_getTopVC(withCurrentVC: presentVC)
    }
    
    else if let tabVC = vc as? UITabBarController {
        // tabBar 的跟控制器
        if let selectVC = tabVC.selectedViewController {
            return ls_getTopVC(withCurrentVC: selectVC)
        }
        return nil
    }
    
    else if let naiVC = vc as? UINavigationController {
        // 控制器是 nav
        return ls_getTopVC(withCurrentVC:naiVC.visibleViewController)
    } // UIPageController
    
    else if let pageVC = vc as? UIPageViewController,
            pageVC.viewControllers?.count == 1 {
        return ls_getTopVC(withCurrentVC: pageVC.viewControllers?.first)
    }
    
    else {
        // 返回顶控制器
        return vc
    }
}
