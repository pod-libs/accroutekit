//
//  AccService.swift
//  Acc
//
//  Created by zhouen on 2021/3/18.
//

import UIKit

@objc public protocol AccServiceProtocol: UIApplicationDelegate {
    static func service() ->Self
}

